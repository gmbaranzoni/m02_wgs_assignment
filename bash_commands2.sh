#!/bin/bash

### Description: in every file, extract all the variants located in one chromosome, add the name of the sample in each row, and merge all the files together 

output_base_name=MIT_CRC_Chr1_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="1" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr2_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="2" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#-------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr3_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="3" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr4_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="4" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr5_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="5" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr6_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="6" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr7_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="7" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr8_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="8" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr9_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="9" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr10_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="10" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr11_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="11" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#---------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr12_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="12" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr13_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="13" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#---------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr14_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="14" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#---------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr15_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="15" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#---------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr16_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="16" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr17_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="17" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr18_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="18" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr19_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="19" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr20_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="20" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr21_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="21" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_Chr22_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="22" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_ChrX_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="X" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


#--------------------------------------------------------------------------------------
output_base_name=MIT_CRC_ChrY_

for vcf in *vcf.gz;
do
	# Extract all the variants tha passed the filter in Chr 1
	zcat $vcf | awk '$0!~/^#/ && $1=="Y" && $7=="PASS"' > ${vcf%%.*}".selected_var.txt"
	
	# Add a new column with the sample label
	sed -i "s/$/\t${vcf%%.*}/" ${vcf%%.*}".selected_var.txt"

done

# merge all the files together
cat *selected_var.txt > "${output_base_name}all.txt"
rm *selected_var.txt

# Sort by the variant position
#sort --key 2 --numeric-sort "${output_base_name}all.txt" > MIT_CRC_Chr1_all_sorted.txt


