#!/bin/bash

# Open empty files
echo "file" > vcf_list.txt
echo "tot_var" > tot_var.txt
echo "pass_var" > pass_var.txt

echo "pass_bi_al_var" > pass_bi_al_var.txt
echo "pass_mu_al_var" > pass_mu_al_var.txt
echo "pass_bi_al_snp" > pass_bi_al_snp.txt

echo "pass_homo_var" > pass_homo_var.txt
echo "pass_hete_var" > pass_hete_vat.txt
echo "unident_geno"  > unident_geno.txt


for vcf in *vcf.gz;
do
	# List of the file
	file_name=${vcf%%.*} 
	echo $file_name >> vcf_list.txt

	# Number of variants in the vcf file
	zcat $vcf | awk '$0!~/^#/ && $1=="18"' | wc -l >> tot_var.txt

	# Number of variants that has passed all the quality filters
      	zcat $vcf | awk '$0!~/^#/ && $1=="18" && $7=="PASS"' | wc -l >> pass_var.txt

	# Number of bi-allelic variants that passed the filter
	zcat $vcf | awk '$0!~/^#/ && $1=="18" && $7=="PASS" && $5!~/,/' | wc -l >> pass_bi_al_var.txt
	
	# Number of multi-allelic variants that passed the filter
	zcat $vcf | awk '$0!~/^#/ && $1=="18" && $7=="PASS" && $5~/,/' | wc -l >> pass_mu_al_var.txt

	# Number of bi-allelic snps that passed the filter
	zcat $vcf | awk '$0!~/^#/ && $1=="18" && $7=="PASS" && $5!~/,/ && length($4)==1 && length($5)==1' | wc -l >> pass_bi_al_snp.txt

	# Number of homozygotes that passed the filter
	zcat $vcf | awk '$0!~/^#/ && $1=="18" && $7=="PASS" && $10~/^1\/1/' | wc -l >> pass_homo_var.txt

	# Number of heterozygotes that passed the filter
	zcat $vcf | awk '$0!~/^#/ && $1=="18" && $7=="PASS" && $10!~/^1\/1/' | wc -l >> pass_hete_vat.txt

	# Number of unidentifiable genotypes
	zcat $vcf | awk '$0!~/^#/ && $1=="18" && $7=="PASS" && $10 ~ /^\.\/\./' | wc -l >> unident_geno.txt 

done


paste vcf_list.txt tot_var.txt pass_var.txt pass_bi_al_var.txt pass_mu_al_var.txt pass_bi_al_snp.txt pass_homo_var.txt pass_hete_vat.txt unident_geno.txt > Chr18_summary.tab

  rm  vcf_list.txt tot_var.txt pass_var.txt pass_bi_al_var.txt pass_mu_al_var.txt pass_bi_al_snp.txt pass_homo_var.txt pass_hete_vat.txt unident_geno.txt




