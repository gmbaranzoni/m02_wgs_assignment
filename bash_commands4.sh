#!/bin/bash

# Open empty files
echo "file" > vcf_list.txt
echo "pass_known_var" > pass_known_var.txt

for vcf in *vcf.gz;
do
	# List of the file
	file_name=${vcf%%.*} 
	echo $file_name >> vcf_list.txt

	# Number of variants in the vcf file
	zcat $vcf | awk '$0!~/^#/' | wc -l >> tot_var.txt

	# Number of bi-allelic variants that passed the filter
	zcat $vcf | awk '$0!~/^#/ && $7=="PASS" && $3!~/\./' | wc -l >> pass_known_var.txt
	
done


paste vcf_list.txt pass_known_var.txt > count_known.tab

  rm  vcf_list.txt pass_known_var.txt




